﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttributeStat {NotAttribute,Power,Precision,Toughness,Vitality}

public class Attributes : MonoBehaviour
{
    public float basePower;
    public float modPower;
    public float basePrecision;
    public float modPrecision;
    public float baseToughness;
    public float modToughness;
    public float baseVitality;
    public float modVitality;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
