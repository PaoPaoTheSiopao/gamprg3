﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StackType { NotStackable,DurationStack,IntensityStack}
public enum BuffType { AttributeBuff,EffectBuff}

public class Buff : MonoBehaviour
{
    public string name;
    public float buffCond;
    public bool withDur;
    private float buffDur;
    public float buffDurI;
    public float curStack = 1;
    public float maxStack;
    private int counter = 1;

    private GameObject source;
    private GameObject target;
    public StackType stack;
    public AttributeStat stat;

    public List<float> durStack;
    public List<float> condStack;
    private void Start()
    {
        TestPlayer t = target.GetComponent<TestPlayer>();
        //Automatically Adds Buff if theres no currently buff on the target
        if (t.buffs.Count <= 0)
        {
            t.buffs.Add(this.gameObject);
            AddBuffToList(this);
            //Debug.Log("DurStacks Start : " + durStack.Count + " = "+durStack[0] );
        }
        else
        {
            //Checks if the buff is stackable
            if (stack != StackType.NotStackable)
            {
                //Checks if theres an existing buff with the same name
                for (int i = 0; i < t.buffs.Count; i++)
                {
                    if (name == t.buffs[i].GetComponent<Buff>().name)
                    {
                        //Debug.Log("Currently Have the Same Buff");
                        CheckStackable(t.buffs[i].GetComponent<Buff>());
                        //Debug.Log("Break");
                        return;
                    }
                }
                //Debug.Log("AddingNew");
                t.buffs.Add(this.gameObject);
                AddBuffToList(this);
            }
            else
            {
                //if the buff is not stackable it will replace the existing buff 
                for (int i = 0; i < t.buffs.Count; i++)
                {
                    if (name == t.buffs[i].GetComponent<Buff>().name)
                    {
                        GameObject temp = t.buffs[i];
                        t.buffs.Remove(t.buffs[i]);
                        Destroy(temp);
                        break;
                    }
                }
                t.buffs.Add(this.gameObject);
                AddBuffToList(this);
            }
        }
    }

    private void Update()
    {
        if(counter==1)
        {
            //one time update is only called once in the update
            //theres an end time update at the removebuff function.
            OneTimeUpdate();
            counter--;
        }
        //Do Effects based On StackType
        if (withDur)
        {
            switch(stack)
            {
                case StackType.DurationStack:
                    if (buffDur > 0)
                    {
                        Effect();
                        buffDur -= Time.deltaTime;
                    }
                    else
                    {
                        CheckDurationStack();
                    }
                    break;
                case StackType.IntensityStack:
                    Effect();
                    buffDurI += Time.deltaTime;
                    CheckDurationStackForCurTime();
                    break;
                case StackType.NotStackable:
                    if (buffDur > 0)
                    {
                        Effect();
                        buffDur -= Time.deltaTime;
                    }
                    else
                    {
                        RemoveBuff();
                    }
                    break;
            }
        }

    }
    protected virtual void CheckDurationStack()
    {
        durStack.RemoveAt(0);
        condStack.RemoveAt(0);
        if (durStack.Count <= 0)
        {
            //Debug.Log("Remove cause of no elementsD");
            RemoveBuff();
        }
        else
        {
            buffDur = durStack[0];
        }
    }
    protected virtual void CheckDurationStackForCurTime()
    {
        for (int s = 0; s < durStack.Count; s++)
        {
            if (durStack[s] - buffDurI <= 0)
            {
                durStack.RemoveAt(s);
                condStack.RemoveAt(s);
                curStack--;
                for (int y = 0; y < durStack.Count; y++)
                {
                    float temp = 0;
                    temp = durStack[y] - buffDurI;
                    durStack[y] = temp;
                }
                if (durStack.Count <= 0)
                {
                    //Debug.Log("Remove cause of no elementsI");
                    //Debug.Log("DurStacks : " + durStack.Count);
                    RemoveBuff();
                }

                buffDurI = 0;
            }
        }
    }
    protected virtual void AddBuffToList(Buff o)
    {
        o.durStack.Add(buffDur);
        o.condStack.Add(buffCond);
    }
    protected virtual void Effect()
    {

    }

    protected virtual void OneTimeUpdate()
    {
        Debug.Log("Doing OneTimeUpdate");
    }

    protected virtual void EndTimeUpdate()
    {

    }
    protected virtual void CheckStackable(Buff obj)
    {
        //Checks If Buff has Max Stacks
        if (obj.curStack >= obj.maxStack)
        {
            RemoveBuff();
        }
        else
        {
            //Stacks Buff Based on StackType
            switch (stack)
            {
                case StackType.DurationStack:
                    //Debug.Log("DurationStack");
                    StackEffect(obj);
                    //Debug.Log("Remove cause of stacking");
                    RemoveBuff();
                    break;
                case StackType.IntensityStack:
                    //Debug.Log("IntensityStack");
                    for (int y = 0; y < obj.durStack.Count; y++)
                    {
                        float temp = 0;
                        temp = obj.durStack[y] - obj.buffDurI;
                        obj.durStack[y] = temp;
                    }
                    obj.buffDurI = 0;
                    StackEffect(obj);
                    //Debug.Log("Remove cause of stacking");
                    RemoveBuff();
                    break;
            }
        }
    }

    protected virtual void StackEffect(Buff obj)
    {
        AddBuffToList(obj);
        obj.curStack++;
    }

    protected virtual void RemoveBuff()
    {
        //calls one time after the buff duration ended
        EndTimeUpdate();
        TestPlayer t = target.GetComponent<TestPlayer>();
        t.buffs.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

    public virtual void SetParameters(GameObject src, GameObject trg, float bufDuration, float bufCondition)
    {
        source = src;
        target = trg;
        buffDur = bufDuration;
        buffCond = bufCondition;
    }

}
