﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayer : MonoBehaviour
{
    public GameObject buff;
    public GameObject buff2;
    public GameObject buff3;
    public List<GameObject> buffs;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject b = Instantiate(buff2, this.transform.position, this.transform.rotation);
            b.transform.SetParent(this.gameObject.transform);
            Buff buf = b.GetComponent<Buff>();
            //Source of the buff, the target, duration, condition 
            buf.SetParameters(this.gameObject, this.gameObject, 10,5);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameObject b = Instantiate(buff, this.transform.position, this.transform.rotation);
            b.transform.SetParent(this.gameObject.transform);
            Buff buf = b.GetComponent<Buff>();
            buf.SetParameters(this.gameObject, this.gameObject, 10,10);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            GameObject b = Instantiate(buff3, this.transform.position, this.transform.rotation);
            b.transform.SetParent(this.gameObject.transform);
            Buff buf = b.GetComponent<Buff>();
            buf.SetParameters(this.gameObject, this.gameObject, 10,2);
        }
    }


}
